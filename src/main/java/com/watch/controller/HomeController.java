package com.watch.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	//home người admin
	@GetMapping({"/","/admin","/admin/watchMan"})
	public String homeAmin() {
		return "redirect:/assets/admin/main/homeAdmin.html";
	}
	
	//Home người dùng
	@GetMapping({"/watchMen","/watchMen/home"})
	public String homeClient() {
		return"/layout/homeClient";
	}
}
