package com.watch.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductController {
	
	//Product
	@GetMapping("/watchMen/product")
	public String sanpham() {
		return"/user/product/sanPham";
	}
	
	//ChitietSp
	@GetMapping("/watchMen/product/detail")
	public String chitietSp() {
		return"/user/product/ChiTietSP";
	}
	
}
