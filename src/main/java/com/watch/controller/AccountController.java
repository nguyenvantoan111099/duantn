package com.watch.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AccountController {

	// Get Cap nhat tai khoan
	@GetMapping("/watchMen/account")
	public String capNhatTaiKhoan() {
		return "/user/account/capNhatTK";
	}

	// Get lichSuMuahang
	@GetMapping("/watchMen/account/history")
	public String lichSuMuahang() {
		return "/user/account/lichSuMuaHang";
	}
	
	// Get sanPhamYeuThich
		@GetMapping("/watchMen/account/favorite")
		public String sanPhamYeuThich() {
			return "/user/account/sanPhamYeuThich";
		}
		
		// Get doiMatKhau
		@GetMapping("/watchMen/account/changePassword")
		public String doiMatKhau() {
			return "/user/account/doiMatKhau";
		}

}
