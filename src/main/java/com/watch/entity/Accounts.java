package com.watch.entity;



import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity 
public class Accounts implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String Username;
	private String Passwords;
	private String Fullname;
	private String Email;
	private String Phone;
	private String Address;
	private Date Birthdate;
	private Date CreateDate;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account")
	private List<Orders> orders;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account")
	private List<WishList> wishLists;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account")
	private List<Feedback> feedbacks;
	
	@JsonIgnore
	@OneToMany(mappedBy = "account")
	private List<Authorities> authorities;
	
	
}
