package com.watch.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity 
public class WishList implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int wishListId;	
	private Date likeDate;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private Accounts account;
	
	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;
		
}
