package com.watch.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
public class Vouchers implements Serializable{
	
	@Id
	@Column(length = 50, nullable = false)
	private String voucherName;
	private double valued;
	private int quantity;
	private double conditions;
	private boolean statustt;
	private String note;
	
	@OneToMany(mappedBy = "voucher")
	private List<Orders> orders;
	
}
