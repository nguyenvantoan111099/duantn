package com.watch.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity 
public class Product implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;
	private String name;
	private String size;
	private String color;
	private int quantity;
	private double price;
	private String origin;
	private String waterproof;
	private Date crateDate;
	private String image;
	private boolean status;
	private String description;

	@ManyToOne
	@JoinColumn(name = "categoryId")
	private Category category;
	
	@ManyToOne
	@JoinColumn(name = "brandId")
	private Brand brand;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product")
	private List<ImageProduct> images;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product")
	private List<WishList> wishLists;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product")
	private List<Feedback> feedbacks;
	
	@JsonIgnore
	@OneToMany(mappedBy = "product")
	private List<OrderDetail> orderDetails;
	
}
