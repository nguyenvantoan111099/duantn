package com.watch.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity 
public class Authorities implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int newsId;	
	
	@ManyToOne
	@JoinColumn(name = "id")
	private Accounts account;
	
	@ManyToOne
	@JoinColumn(name = "roleId")
	private Roles role;
}
