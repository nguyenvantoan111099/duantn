package com.watch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WatchMenApplication {

	public static void main(String[] args) {
		SpringApplication.run(WatchMenApplication.class, args);
	}

}
